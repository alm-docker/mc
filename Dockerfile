FROM multiarch/alpine:_DISTCROSS-edge
MAINTAINER Adrien le Maire <adrien@alemaire.be>
ARG MC_VERSION
RUN apk add --update --no-cache ca-certificates curl && \
    curl -LOs https://dl.min.io/client/mc/release/linux-_GOCROSS/archive/mc.${MC_VERSION} && \
    mv mc.${MC_VERSION} /usr/bin/mc && chmod +x /usr/bin/mc

ENTRYPOINT ["/usr/bin/mc"]
